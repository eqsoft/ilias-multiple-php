FROM debian:stretch-slim

LABEL maintainer="schneider@hrz.uni-marburg.de"

ENV DEBIAN_FRONTEND noninteractive

# install NGINX
RUN apt-get update && \
	apt-get install -y nginx --no-install-recommends && \
	rm -rf /var/lib/apt/lists/*

# add sury.org repo
RUN apt-get update && \
    apt-get install -y \ 
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg --no-install-recommends && \
	rm -rf /var/lib/apt/*
RUN curl https://packages.sury.org/php/apt.gpg | apt-key add -
RUN echo 'deb https://packages.sury.org/php/ stretch main' > /etc/apt/sources.list.d/deb.sury.org.list

# add misc packages
RUN apt-get update && \
    apt-get install -y \
    imagemagick \
    libxslt1-dev \
    zip \
    unzip \
    libpng-dev 

# install PHP 5.6
RUN apt-get update && \
	apt-get install -y \
    php5.6-cli \
    php5.6-common \
    php5.6-fpm \
    php5.6-gd \
    php5.6-xsl \
    php5.6-xml \
    php5.6-gettext \
    php5.6-mysql \
    --no-install-recommends && \
	rm -rf /var/lib/apt/lists/*

# install PHP 7.0
RUN apt-get update && \
	apt-get install -y \
    php7.0-cli \
    php7.0-common \
    php7.0-fpm \
    php7.0-gd \
    php7.0-xsl \
    php7.0-xml \
    php7.0-gettext \
    php7.0-mysql \
    php7.0-mysqli \
    --no-install-recommends && \
	rm -rf /var/lib/apt/lists/*

# install PHP 7.1
RUN apt-get update && \
	apt-get install -y \
    php7.1-cli \
    php7.1-fpm --no-install-recommends && \
	rm -rf /var/lib/apt/lists/*
    
# install PHP 7.2
RUN apt-get update && \
	apt-get install -y \
    php7.2-cli \
    php7.2-fpm --no-install-recommends && \
	rm -rf /var/lib/apt/lists/*


# install PHP 7.3
RUN apt-get update && \
	apt-get install -y \
    php7.3-cli \
    php7.3-fpm --no-install-recommends && \
	rm -rf /var/lib/apt/lists/*

# verify versions
RUN php5.6 -v
RUN php7.0 -v
RUN php7.1 -v
RUN php7.2 -v
RUN php7.3 -v
RUN php -v

# clear active virtual hosts
RUN rm -f /etc/nginx/sites-enabled/*

# prepare PHP 5.6 virtual host
RUN mkdir /var/www/site-with-php5.6
COPY index.php /var/www/site-with-php5.6/index.php
COPY site-with-php5.6.vhost /etc/nginx/sites-available/site-with-php5.6

# prepare PHP 7.0 virtual host
RUN mkdir /var/www/site-with-php7.0
COPY index.php /var/www/site-with-php7.0/index.php
COPY site-with-php7.0.vhost /etc/nginx/sites-available/site-with-php7.0

# prepare PHP 7.1 virtual host
RUN mkdir /var/www/site-with-php7.1
COPY index.php /var/www/site-with-php7.1/index.php
COPY site-with-php7.1.vhost /etc/nginx/sites-available/site-with-php7.1

# prepare PHP 7.2 virtual host
RUN mkdir /var/www/site-with-php7.2
COPY index.php /var/www/site-with-php7.2/index.php
COPY site-with-php7.2.vhost /etc/nginx/sites-available/site-with-php7.2

# prepare PHP 7.3 virtual host
RUN mkdir /var/www/site-with-php7.3
COPY index.php /var/www/site-with-php7.3/index.php
COPY site-with-php7.3.vhost /etc/nginx/sites-available/site-with-php7.3

# enable the virtual hosts
RUN ln -s ../sites-available/site-with-php5.6 /etc/nginx/sites-enabled
RUN ln -s ../sites-available/site-with-php7.0 /etc/nginx/sites-enabled
RUN ln -s ../sites-available/site-with-php7.1 /etc/nginx/sites-enabled
RUN ln -s ../sites-available/site-with-php7.2 /etc/nginx/sites-enabled
RUN ln -s ../sites-available/site-with-php7.3 /etc/nginx/sites-enabled

# (Docker-specific) install supervisor so we can run everything together
RUN apt-get update && \
	apt-get install -y supervisor --no-install-recommends && \
	rm -rf /var/lib/apt/lists/*
COPY supervisor.conf /etc/supervisor/supervisord.conf
RUN mkdir /run/php

EXPOSE 8856 8870 8871 8872 8873
CMD ["supervisord", "-c", "/etc/supervisor/supervisord.conf"]
